const express = require('express');
const router = express.Router();
const api = require('../api/api_post');
const multer = require('multer');
const path = require('path');
var cloudinary = require('cloudinary');

cloudinary.config({ 
	cloud_name: 'dpglbnu22', 
	api_key: '692846919168666', 
	api_secret: 'jlLOEqH1WD-BzuC4weBXeG-f9g4' 
  });
  let name;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/')
  },
  filename: function (req, file, cb) {
	  name = Date.now();
    cb(null, name + path.extname(file.originalname))
  }
})

const upload = multer({ storage: storage });

router.post('/upload_post', upload.single('imageName'), async(req,res) => {
	console.log(req.file)
	cloudinary.v2.uploader.upload(req.file.path, {public_id : name},
  function(error, result) {console.log(result, error)});
	try{
		let upPostResponse = await api.upload_post(req.body,req.file);
		console.log(upPostResponse);
		res.send(upPostResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});


router.post('/fetch_post', async(req,res) => {
	try{
		let fetchPostResponse = await api.fetch_post(req.body);
		console.log(fetchPostResponse);
		res.send(fetchPostResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});

router.post('/likes_handler', async(req,res) => {
	try{
		let likesHandlerResponse = await api.likes_handler(req.body);
		console.log(likesHandlerResponse);
		res.send(likesHandlerResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});

router.post('/comment_handler', async(req,res) => {
	try{
		let commentHandlerResponse = await api.comment_handler(req.body);
		console.log(commentHandlerResponse);
		res.send(commentHandlerResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});

router.post('/fetch_comment', async(req,res) => {
	try{
		let fetchCommentResponse = await api.fetch_comment(req.body);
		console.log(fetchCommentResponse);
		res.send(fetchCommentResponse);
	}
	catch(err)
	{
		res.send('err');
	}
});

module.exports = router;