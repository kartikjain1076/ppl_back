const express = require('express');
const router = express.Router();
const api = require('../api/api_category');
const multer = require('multer');
const upload = multer({ dest: '../ppl_front/public/' });

router.post('/upload_category', upload.single('imageName'), async(req,res) => {
	try{
		let up_category_response = await api.upload_category(req.body,req.file);
		console.log(up_category_response);
		res.send(up_category_response);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});


router.post('/fetch_category', async(req,res) => {
	try{
		let fetchCategoryResponse = await api.fetch_category(req.body);
		console.log(fetchCategoryResponse);
		res.send(fetchCategoryResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});

module.exports = router;