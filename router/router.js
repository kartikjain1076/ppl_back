const express = require('express');
const router = express.Router();
const api = require('../api/api');
const multer = require('multer');

router.post('/register', async (req,res) => {
	console.log(req.body);

	try{
		let registerResponse = await api.register(req.body);
		console.log(registerResponse);
		res.send(registerResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});


router.post('/login', async (req,res) => {
	console.log(req.body);
	try{
		let loginResponse = await api.login(req.body);
		console.log(loginResponse);
		res.send(loginResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});


router.post('/verify', async (req,res) => {
	try{
		let verifyResponse = await api.verify(req.body);
		console.log(verifyResponse);
		res.send(verifyResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});

router.post('/forgot_password', async(req,res) => {
	try{
		let forgotPasswordResponse = await api.forgot_password(req.body);
		console.log(forgotPasswordResponse);
		res.send(forgotPasswordResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});

router.post('/new_password', async(req,res) => {
	try{
		let newPasswordResponse = await api.new_password(req.body);
		console.log(newPasswordResponse);
		res.send(newPasswordResponse);
	}
	catch(err)
	{
		console.log(err);
		res.send('err');
	}
});




module.exports = router;