var nodemailer = require('nodemailer');
var register_schema = require('../schema/register_schema');
var Promise = require('promise');

module.exports = {

    verify : function(data) {

        return new Promise((resolve,reject) => {
            register_schema.findOneAndUpdate({_id : data.id},{verified : true},function(err,result){
                if(err)
                {
                    console.log(err);
                    reject(err);
                }
                else
                {
                    console.log(result);
                    resolve('ok');
                }
            })
        })
    },

    login : function(data) {
        return new Promise((resolve,reject) => {
            register_schema.find({email : data.email}, function(err,result){
                if(err)
                {
                    console.log(err);
                    reject('err');
                }
                else
                {
                    console.log(result);
                    if(result.length > 0)
                    {
                        register_schema.find({email : data.email , password : data.password}, function(err,result){
                            if(err)
                            {
                                console.log(err);
                                reject('err');
                            }
                            if(result.length > 0){
                                if(result[0].verified == false)
                                {
                                    console.log('not_verified');
                                    resolve('not_verified');
                                }
                                else{
                                    var response = {res : 'ok', data : result}
                                    resolve(response);
                                }
                            }
                            else{
                                resolve('incorrect_password');
                            }
                        })
                    }
                    else
                        resolve('no');
                }
            })
        })




    },

    register : function(data) {
        return new Promise((resolve,reject) => {
            register_schema.find({email : data.email},function(err,result){
                if(err)
                {
                    console.log(err);
                    reject('err');
                }
                else if(result.length > 0)
                {
                    console.log('user exists');
                    resolve('exists');
                }
                else
                {
                    register_schema.create({username : data.username, password : data.password, email : data.email, fname : data.fname, lname : data.lname} , function(err,result){
                        if(err)
                        {
                            console.log(err);
                            reject('err');
                        }
                        else
                        {
                            console.log(result);
                            let transporter = nodemailer.createTransport({
                                service:'Gmail',
                                  secure: true, 
                                  auth: {
                                      user: 'kartikjain1@mrei.ac.in',
                                      pass: 'justjoking'
                                  }
                              });
                              let mailOptions = {
                                  from : 'kartikjain1@mrei.ac.in', 
                                  to : result.email, 
                                  subject : 'verification code', 
                                  text : 'Please verify your mail id by click this link : http://localhost:3000/verify/'+result.id
                              };
                              transporter.sendMail(mailOptions, (error, info) => {
                                  if (error) {
                                      return console.log("error mail",error);
                                  }
                                  else
                                  console.log('Message %s sent: %s', info.messageId, info.response);
                              });
                            resolve('ok')
                        }
                    })
                }
            })
        })
    },

    forgot_password : function(data){
        return new Promise((resolve,reject) => {
            register_schema.find({email : data.email},function(err,result){
                if(err)
                {
                    console.log(err);
                    reject('err');
                }
                else
                {
                    if(result.length > 0)
                    {
                        console.log(result);
                        let transporter = nodemailer.createTransport({
                            service:'Gmail',
                              secure: true, 
                              auth: {
                                  user: 'kartikjain1@mrei.ac.in',
                                  pass: 'justjoking'
                              }
                          });
                          let mailOptions = {
                              from : 'kartikjain1@mrei.ac.in', 
                              to : result[0].email, 
                              subject : 'Forgot Password', 
                              text : 'Please change your password by click this link : http://localhost:3000/new_password/'+result[0].id
                          };
                          transporter.sendMail(mailOptions, (error, info) => {
                              if (error) {
                                  return console.log("error mail",error);
                              }
                              else
                              console.log('Message %s sent: %s', info.messageId, info.response);
                          });
                          resolve('ok');
                    }
                    else
                    {
                        console.log('mail id not found');
                        resolve('not_exists');
                    }
                }
            })
        }
    )},

    new_password : function(data) {
        return new Promise((resolve,reject) => {
            register_schema.findOneAndUpdate({_id : data.id},{password : data.password},function(err,result){
                if(err)
                {
                    console.log(err)
                    reject('err');
                }
                else
                {
                    console.log(result);
                    resolve('ok');
                }
            })
        })
    }

    
    }













