var Promise = require('promise');
var category_schema = require('../schema/category_schema');

module.exports = {
upload_category : function(data,data_file){
    return new Promise((resolve,reject) => {
        category_schema.create({imageName : data_file.filename, category : data.category}, function(err,result){
            if(err)
            {
                console.log(err);
                reject('err');
            }
            else
            {
                console.log(result);
                resolve('ok');
            }
        })
    })
},

fetch_category : function(data){
    return new Promise((resolve,reject) => {
        category_schema.find({}, function(err,result){
            if(err)
            {
                console.log(err);
                reject('err');
            }
            else
            {
                let a = {re : 'ok', body : result};
                console.log(result);
                resolve(a);
            }
        })
    })
}
}