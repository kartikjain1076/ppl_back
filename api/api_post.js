const Promise = require('promise');
const post_schema = require('../schema/post_schema');
const fs = require('fs');

module.exports = {
upload_post : function(data,dataFile){
    console.log(data);
    let d = new Date;
    let day = d.getDate();
    let m = d.getMonth();
    let month = ['January','Febuary','March','April','May','June','July','August','September','October','November','December'];
    month = month[m];
    let year = d.getFullYear();
    let hours = d.getHours();
    let minutes = d.getMinutes();
    let amorpm = 'AM'
    if(hours > 12)
    {
        amorpm = 'PM';
        hours = hours - 12;
    }
    d = day + ' ' + month + ' ' + year + '        ' + hours + ':' + minutes + ' ' + amorpm;
    return new Promise((resolve,reject) => {
        console.log(data.imageName);
        post_schema.create({ imageName : dataFile.filename,  date : d, fname : data.fname, lname : data.lname, username : data.username, category : data.category, caption : data.caption}, function(err,result){
            if(err)
            {
                console.log(err);
                reject('err');
            }
            else
            {
                console.log(result);
                resolve('ok');
            }
        })
    })
},

fetch_post : function(data){
    return new Promise((resolve,reject) => {
        post_schema.find({}, function(err,result){
            if(err)
            {
                console.log(err);
                reject('err');
            }
            else
            {
                let b = {re : 'ok', body : result};
                console.log(result);
                resolve(b);
            }
        })
    })
},



likes_handler : function(data){
    return new Promise((resolve,reject) => {
        post_schema.find({_id:data.id}, function(err,result){
            if(err)
            {
                console.log(err);
                reject('err');
            }
            else
            {
                var check = result[0].likes.indexOf(data.id2);
                if(check == -1)                    
                {
                    result[0].likes.push(data.id2);
                    console.log(result[0].likes);
                    post_schema.findOneAndUpdate({_id:data.id},{likes : result[0].likes},function(err,result){
                        if(err)
                        {
                            console.log(err);
                            reject('err');
                        }
                        else
                        {
                            console.log(result);
                            resolve('ok');
                        }
                    })
                }
                else
                {
                    result[0].likes.splice(check, 1);
                    post_schema.findOneAndUpdate({_id:data.id},{likes : result[0].likes},function(err,result){
                        if(err)
                        {
                            console.log(err);
                            reject('err');
                        }
                        else
                        {
                            console.log(result);
                            resolve('ok');
                        }
                    })
                }
                
            }
        })
    })
},

comment_handler : function(data) {
    return new Promise((resolve,reject) => {
        post_schema.find({_id : data.id},function(err,result){
            if(err)
            {
                console.log(err);
                reject(err);
            }
            else
            {
                var c = {username : data.username, comment : data.comment};
                var comment = result[0].comment;
                comment.push(c);
                console.log('comment',comment);
                post_schema.findOneAndUpdate({_id : data.id},{comment : comment},function(err,result){
                    if(err)
                    {
                        console.log(err);
                        reject('err');
                    }
                    else
                    {
                        console.log(result);
                        resolve('ok');
                    }
                })
            }
        })
    })
},

fetch_comment : function(data){
    return new Promise((resolve,reject) => {
        post_schema.findOne({_id :data.id }, function(err,result){
            if(err)
            {
                console.log(err);
                reject('err');
            }
            else
            {
                let b = {re : 'ok', body : result.comment};
                console.log(result);
                resolve(b);
            }
        })
    })
}
}