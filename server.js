const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const bodyparser = require('body-parser');
app.use(cors());

const router = require('./router/router');
const router_category = require('./router/router_category');
const router_post = require('./router/router_post');


app.use(bodyparser.urlencoded({extended : true}));
app.use(bodyparser.json());

mongoose.connect('mongodb://127.0.0.1:27017/ppl',{ useNewUrlParser: true });

app.use('/post/',router_post);
app.use('/category/',router_category);
app.use('/',router);

const port = process.env.port || 5000;

app.listen(port,function(){
	console.log('server started at: 5000');
})