const mongoose = require('mongoose');

const category = mongoose.Schema({
	imageName : String,
    category : String
});

module.exports = mongoose.model('category',category);